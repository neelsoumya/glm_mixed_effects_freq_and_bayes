# glm_mixed_effects_freq_and_bayes

GLM and GLM mixed effects model examples

    * Frequentist, and
    * Bayesian version in Stan (rstanarm)
    
Adapted from: 

    http://www.stat.columbia.edu/~gelman/arm/examples/roaches/logmodels.R
    http://webcache.googleusercontent.com/search?q=cache:PTG2p8co2z8J:mc-stan.org/rstanarm/articles/count.html+&cd=3&hl=en&ct=clnk&gl=uk&client=firefox-b-d
    
    